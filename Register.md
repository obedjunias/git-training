## How to register on Gitlab

1. Open browser go to gitlab.iotiot.in
2. Click on Register Tab
3. Fill out the form and click on register

Done you will be registered 

## How to Access the On-boarding project

1. Open browser go to gitlab.iotiot.in
2. Sign-in 
3. Goto Groups --> Explore Groups

![](/extras/001.png)

![](/extras/002.png)

4. Goto Newbies 

![](/extras/003.png)

5. Request Access

![](/extras/004.png)

### Your request will be approved by the Admin. May take upto 24 hours for approval. Please be patient.